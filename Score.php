<?php

	$root = "/var/www/html/";

require_once $root.'mega-cron-job/Config.php';
require_once $root.'mega-cron-job/DB.php';
require_once $root.'mega-cron-job/vendor/autoload.php';
require_once $root.'mega-cron-job/MenuLogger.php';
require_once $root.'mega-cron-job/WritableDB.php';
require_once $root.'mega-cron-job/SMSConfig.php';
require_once $root.'mega-cron-job/SMS.php';

use Carbon\Carbon;

/**
* 
*/

class Score
	{
		public $logger;
		
		function __construct()
			{
				$this->logger = new MenuLogger(Config::loggingDir,Config::infoLog, Config::errorLog);

				$file = $_SERVER['SCRIPT_FILENAME'];

				$ps = "ps aux|grep -v grep|grep $file -c"; 

				$shell = shell_exec($ps);

				echo "shell ".$shell;

				if ((int) $shell > 3)
					{
                        $this->logger->ALERT(" Poller is already running with these details: $file | $shell try next time...", __LINE__, __FUNCTION__);
						exit(" Poller is already running with these details: $file | $shell try next time...");
					}
			}

    /**
	 * Get profile score
	 *
     * @param integer $hour
     * @param string $date
     */

		public function profileScore($date,$hour)
		{
			$this->logger->ALERT(__FUNCTION__.'.'.__LINE__." JOB started profileScore $date - $hour");

			$t1 = $this->getTime();

			$deposit_score_sql = "SELECT SUM(mp.mpesa_amt) as total_deposit, p.id as profile_id,:hour as hour,:date as date, '0' as total_tickets,'0' as ticket_score, '0' as deposit_score,'0' as total_stake FROM mpesa_transaction mp inner join profile p ON mp.msisdn=p.msisdn WHERE date(mp.created) = :date AND HOUR(mp.created) = :hour GROUP BY p.id";

            $ticket_score_sql = "select x.id as profile_id,x.tickets total_tickets,((x.tickets * 416/891) - (x.stake/44550) ) as ticket_score,:hour as hour,:date as date,x.stake as total_stake,'0' as deposit_score, '0' as total_deposit from (select sum(abs(t.amount)) as stake,p.id,count(t.id) as tickets from profile p inner join  transaction t on p.id=t.profile_id  where date(t.created) = :date and hour(t.created) = :hour AND t.transaction_type_id = 2 AND t.reference_type_id IN (1,7,8) group by p.id)x";

            $pr = array(':date'=>$date,':hour'=>$hour);


			$deposit_score_results = DB::executeFetchStatement($deposit_score_sql,$pr,__FILE__.".".__LINE__);

            $ticket_score_results = DB::executeFetchStatement($ticket_score_sql,$pr,__FILE__.".".__LINE__);

            $params = array();
            foreach ($deposit_score_results as $key=>$row){

            	$param = array(
            		':profile_id'=>$row->profile_id,
                    ':deposit_score'=>$row->deposit_score,
                    ':total_deposit'=>$row->total_deposit,
                    ':total_stake'=>$row->total_stake,
                    ':hour'=>$row->hour,
                    ':date'=>$row->date,
                    ':total_ticket'=>$row->total_tickets,
                    ':ticket_score'=>$row->ticket_score,
                );

            	$params[] = $param;

			}

            foreach ($ticket_score_results as $key=>$row){

            	$index = $this->inArray($params,':profile_id',$row->profile_id);
            	if($index > -1){

					$params[$index][':total_ticket'] = $row->total_tickets;
                    $params[$index][':ticket_score'] = $row->ticket_score;

                    if($row->total_stake > 0 && $params[$index][':total_deposit'] > 0){
                    	$deposit_score = ceil($row->total_stake / $params[$index][':total_deposit'] * 100);
                        $params[$index][':deposit_score'] = $deposit_score;
                    }

                    $params[$index][':total_stake'] = $row->total_stake;

                }
				else {
                    $param = array(
                        ':profile_id'=>$row->profile_id,
                        ':deposit_score'=>$row->deposit_score,
                        ':total_deposit'=>$row->total_deposit,
                        ':total_ticket'=>$row->total_tickets,
                        ':ticket_score'=>$row->ticket_score,
                        ':total_stake'=>$row->total_stake,
                        ':hour'=>$row->hour,
                        ':date'=>$row->date,
                    );

                    $params[] = $param;
                }
            }


			$insert = "INSERT INTO profile_score(profile_id,ticket_score,deposit_score,total_deposit,total_ticket,total_stake,hour,date) VALUES (:profile_id,:ticket_score,:deposit_score,:total_deposit,:total_ticket,:total_stake,:hour,:date) ON DUPLICATE KEY UPDATE ticket_score=VALUES(ticket_score),deposit_score=VALUES(deposit_score),total_deposit=VALUES(total_deposit),total_ticket=VALUES(total_ticket),total_stake=VALUES(total_stake)";


			foreach($params as $row=>$values){
                DB::executeInsertStatement($insert,$values,__FILE__.".".__LINE__);
			}

			$t2 = $this->getTime();

			$timeTaken = $t2 - $t1;

			$this->logger->ALERT(__FUNCTION__.'.'.__LINE__." JOB ended profileScore  $date - $hour time taken $timeTaken s ");

		}

    	public function getTime() {
			list($usec, $sec) = explode(" ", microtime());
			return ((float) $usec + (float) $sec);
		}

		public function inArray($array, $key,$value){

			foreach($array as $ky=>$v){
				if($v[$key] === $value){
                    return $ky;
				}
			}

			return -1;

		}
}

    parse_str(implode('&', array_slice($argv, 1)), $_GET);

if(isset($_GET['day'])){
    $start = new Carbon();
    $start->setDate(2017,9,3);
    $day = $_GET['day'];
    $currentDate = $start->addDay(intval($day));
    $date = $currentDate->toDateString();
    $hour = 0;
    $job = new Score();

    $t1 = $job->getTime();

    while($hour < 24){
        $job->profileScore($date, $hour);
        sleep(10);
        $hour++;
    }

    $x = $job->getTime();

    $x = $x - $t1;

    $x = number_format($x/60);

    $xx = "$x MINUTES ".($x%60)." SECONDS ";

    $logger = new MenuLogger(Config::loggingDir,Config::infoLog, Config::errorLog);

    $logger->ALERT(__FUNCTION__.'.'.__LINE__." day $day date $date JOB ended profileScore time taken $xx MINs");
}
		else {
            $date = isset($_GET['date']) ? $_GET['date'] : date('Y-m-d');
            $hour = isset($_GET['hour']) ? $_GET['hour'] : date('H');
            $job = new Score();
            $job->profileScore($date, $hour);
        }

?>
