<?php
    /**
     * Created by PhpStorm.
     * User: mukuha
     * Date: 11/22/17
     * Time: 10:37 AM
     */

    require_once __DIR__ . '/vendor/autoload.php';

    define('APPLICATION_NAME', 'SOUTHWELL_STATS_BOTS_REPORTS');
    define('CREDENTIALS_PATH', '~/.credentials/drive-php-quickstart.json');
    define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json');
    define('SCOPES', implode(' ', array(Google_Service_Drive::DRIVE_METADATA_READONLY)));


    class GDriver
    {

        public $message = "Sample message";
        
        public function __construct()
        {
            if (php_sapi_name() != 'cli') {
                throw new Exception('This application must be run on the command line.');
            }
        }

        public function execute()
        {

            $chunkSizeBytes = 1 * 1024 * 1024; // size of chunks we are going to send to google

            // code to create mime message
            $googleClient = $this->getClient();


            // code to setup the client
            $mailService = new Google_Service_Gmail($googleClient);
            $message = new Google_Service_Gmail_Message();

            // Call the API with the media upload, defer so it doesn't immediately return.
            $googleClient->setDefer(true);
            $result = $mailService->users_messages->send('me', $message);

            // create mediafile upload
            $media = new Google_Http_MediaFileUpload(
                $googleClient,
                $result,
                'message/rfc822',
                $this->message,
                true,
                $chunkSizeBytes
            );

            $media->setFileSize(strlen($this->message));
            // Upload the various chunks. $status will be false until the process is complete.
            $status = false;
            while(!$status) {
                $media->nextChunk();
            }
            // Reset to the client to execute requests immediately in the future.
            $googleClient->setDefer(false);
            $googleMessageId = $status->getId();
        }

        /**
         * Returns an authorized API client.
         *
         * @return Google_Client the authorized client object
         */
        public function getClient()
        {
            $client = new Google_Client();
            $client->setApplicationName(APPLICATION_NAME);
            $client->setScopes(SCOPES);
            $client->setAuthConfig(CLIENT_SECRET_PATH);
            $client->setAccessType('offline');

            // Load previously authorized credentials from a file.
            $credentialsPath = $this->expandHomeDirectory(CREDENTIALS_PATH);

            if (file_exists($credentialsPath)) {
                $accessToken = json_decode(file_get_contents($credentialsPath), true);
            }
            else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

                // Store the credentials to disk.
                if (!file_exists(dirname($credentialsPath))) {
                    mkdir(dirname($credentialsPath), 0700, true);
                }
                file_put_contents($credentialsPath, json_encode($accessToken));
                printf("Credentials saved to %s\n", $credentialsPath);
            }
            $client->setAccessToken($accessToken);

            // Refresh the token if it's expired.
            if ($client->isAccessTokenExpired()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
            }
            return $client;
        }

        /**
         * Expands the home directory alias '~' to the full path.
         *
         * @param string $path the path to expand.
         *
         * @return string the expanded path.
         */
        public function expandHomeDirectory($path)
        {
            $homeDirectory = getenv('HOME');
            if (empty($homeDirectory)) {
                $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
            }
            return str_replace('~', realpath($homeDirectory), $path);
        }

    }



    $job = new GDriver();
    $job->execute();