<?php

/**
 * 
 */
class SMS {

    /**
     * This function sends sms to customer
     * 
     * @param type $headers
     * @param type $postData
     * @return type
     */
    public static function send($msisdn, $message) {
        $logger = new MenuLogger(Config::loggingDir, Config::infoLog, Config::errorLog);

        $postData = array(
            "shortCode" => SMSConfig::SourceAddress,
            "message" => $message,
            "contacts" => array(
                "reciepients" => $msisdn
            ),
            "apiKey" => SMSConfig::apiKey
        );

          $username = SMSConfig::channelUSER;
          $password = SMSConfig::channelPWD;

          $httpRequest = curl_init(SMSConfig::channelAPIURL);
          curl_setopt($httpRequest, CURLOPT_NOBODY, true);
          curl_setopt($httpRequest, CURLOPT_POST, true);
          curl_setopt($httpRequest, CURLOPT_POSTFIELDS, json_encode($postData));
          curl_setopt($httpRequest, CURLOPT_TIMEOUT, 10);
          curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
          'Content-Length: ' . strlen(json_encode($postData))));
          curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
          curl_setopt($httpRequest, CURLOPT_USERPWD, "$username:$password");
          $results = curl_exec($httpRequest);
          $httpStatusCode = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
          curl_close($httpRequest);

        $response = array(
            'httpStatus' => $httpStatusCode,
            'response' => json_decode($results)
        );

        $logger->ALERT("MEGA SMS - TO $msisdn response ".json_encode($response));

        return $response;
    }
}

?>