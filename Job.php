<?php

    $root = "/var/www/html/";

    require_once $root . 'coke-cron-job/Config.php';
    require_once $root . 'coke-cron-job/DB.php';
    require_once $root . 'coke-cron-job/vendor/autoload.php';
    require_once $root . 'coke-cron-job/MenuLogger.php';
    require_once $root . 'coke-cron-job/WritableDB.php';
    require_once $root . 'coke-cron-job/SMSConfig.php';
    require_once $root . 'coke-cron-job/SMS.php';

    use Carbon\Carbon;

    /**
     *
     */
    class Job
    {
        public $logger;

        function __construct()
        {
            $this->logger = new MenuLogger(Config::loggingDir, Config::infoLog, Config::errorLog);

            $root = "/var/www/html/";

            $file = $root . 'coke-cron-job/'.$_SERVER['SCRIPT_FILENAME'];

            $ps = "ps aux|grep -v grep|grep $file -c";

            $shell = shell_exec($ps);

            echo "running instances " . $shell;

            if ((int)$shell > 2) {
                $this->logger->ALERT(" Poller is already running with these details: $file | $shell try next time...", __LINE__, __FUNCTION__);
                exit(" Poller is already running with these details: $file | $shell try next time...");
            }
        }

        public function getTime()
        {
            list($usec, $sec) = explode(" ", microtime());
            return ((float)$usec + (float)$sec);
        }

        public function execute()
        {
            $base_query = "select w.winner_id,w.entry_id, e.code_value,p.msisdn as mobile_number,rt.reward_type, rt.amount, rt.reward_transaction_id, rd.transaction_id, rd.response_description, r.region_name,b.brand_name,p.network,rd.response_status,w.created as winner_time, rd.created as reward_time from winner w left join reward_transaction rt on rt.winner_id = w.winner_id left join reward_dlr rd on rd.reward_transaction_id = rt.reward_transaction_id left join entry e on e.entry_id = w.entry_id left join brand b on b.brand_id = e.brand_id left join profile p on p.profile_id = e.profile_id left join region r on r.region_id = e.region_id where date(w.created) = date(now() - interval 1 day) order by w.winner_id DESC";
            // create file

            $fileName = "daily_winners_report_" . time('Y-m-d H:i:s') . "_" . rand(6, 10) . ".csv";

            $fileName = str_replace(' ', '_', $fileName);
            $fileName = str_replace(':', '_', $fileName);
            $fileError = "/var/www/html/coke-cron-job/reports/temp/$fileName.log";
            $filePath = "/var/www/html/coke-cron-job/reports/$fileName";
            $filePath_header = "/var/www/html/coke-cron-job/reports/temp/header_$fileName";
            $filePath_body = "/var/www/html/coke-cron-job/reports/temp/body_$fileName";

            $csv_row = array();

            $csv_row[] = array("winner_id", "entry_id", "code_value", "mobile_number", "reward_type", "amount", "reward_transaction_id","transaction_id","response_description","region_name","brand_name","network","response_status","winner_time","reward_time");

            $file = fopen($filePath_header, 'w');

            foreach ($csv_row as $line) {
                fputcsv($file, $line);
            }

            // execute query
            $cmd = "echo \"$base_query\" | mysql -ucoke -p'c0ke@p@ss' -h104.199.25.225 fungua_mamilli 2>$fileError -N|sed 's/\t/,/g'>$filePath_body ";

            exec($cmd, $output);

            $res = file_get_contents($fileError);
            $res = trim($res);
            $res = str_replace("\n", "", $res);

            $this->logger->INFO("cokeREPORT CMD " . $cmd . " response: $res ");

            if (strlen($res) > 0 || $res != "") {
                unlink($filePath_header);
                unlink($filePath_body);
                unlink($fileError);
                $this->logger->ERROR($res);
            }
            else {

                shell_exec("cat $filePath_header $filePath_body > $filePath");
                unlink($filePath_header);
                unlink($filePath_body);
                unlink($fileError);

                $paths[] = $filePath;
            }


            $base_query = "select i.inbox_id,p.msisdn,i.message,p.network,i.created from inbox i left join profile p on p.profile_id = i.profile_id where date(i.created) = date(now() - interval 1 day)";
            // create file
            $fileName = "daily_inbox_report_" . time('Y-m-d H:i:s') . "_" . rand(6, 10) . ".csv";

            $fileName = str_replace(' ', '_', $fileName);
            $fileName = str_replace(':', '_', $fileName);
            $fileError = "/var/www/html/coke-cron-job/reports/temp/$fileName.log";
            $filePath = "/var/www/html/coke-cron-job/reports/$fileName";
            $filePath_header = "/var/www/html/coke-cron-job/reports/temp/header_$fileName";
            $filePath_body = "/var/www/html/coke-cron-job/reports/temp/body_$fileName";

            $csv_row = array();

            $csv_row[] = array("inbox_id", "msisdn", "message", "network", "created");

            $file = fopen($filePath_header, 'w');

            foreach ($csv_row as $line) {
                fputcsv($file, $line);
            }

            // execute query
            $cmd = "echo \"$base_query\" | mysql -ucoke -p'c0ke@p@ss' -h104.199.25.225 fungua_mamilli 2>$fileError -N|sed 's/\t/,/g'>$filePath_body ";

            exec($cmd, $output);

            $res = file_get_contents($fileError);
            $res = trim($res);
            $res = str_replace("\n", "", $res);

            $this->logger->INFO("cokeREPORT CMD " . $cmd . " response: $res ");

            if (strlen($res) > 0 || $res != "") {
                unlink($filePath_header);
                unlink($filePath_body);
                unlink($fileError);
                $this->logger->ERROR($res);
            }
            else {
                shell_exec("cat $filePath_header $filePath_body > $filePath");
                unlink($filePath_header);
                unlink($filePath_body);
                unlink($fileError);
                $paths[] = $filePath;
            }

            $base_query = "select e.entry_id,p.msisdn,e.code_value,p.network,b.brand_name,r.region_name,e.created from entry e left join profile p on p.profile_id = e.profile_id left join brand b on b.brand_id = e.brand_id left join region r on r.region_id = e.region_id where date(e.created)=date(now() - interval 1 day)";
            // create file
            $fileName = "daily_entries_report_" . time('Y-m-d H:i:s') . "_" . rand(6, 10) . ".csv";

            $fileName = str_replace(' ', '_', $fileName);
            $fileName = str_replace(':', '_', $fileName);
            $fileError = "/var/www/html/coke-cron-job/reports/temp/$fileName.log";
            $filePath = "/var/www/html/coke-cron-job/reports/$fileName";
            $filePath_header = "/var/www/html/coke-cron-job/reports/temp/header_$fileName";
            $filePath_body = "/var/www/html/coke-cron-job/reports/temp/body_$fileName";

            $csv_row = array();

            $csv_row[] = array("entry_id", "msisdn", "code_value", "network","brand_name","region_name","created");

            $file = fopen($filePath_header, 'w');

            foreach ($csv_row as $line) {
                fputcsv($file, $line);
            }

            // execute query
            $cmd = "echo \"$base_query\" | mysql -ucoke -p'c0ke@p@ss' -h104.199.25.225 fungua_mamilli 2>$fileError -N|sed 's/\t/,/g'>$filePath_body ";

            exec($cmd, $output);

            $res = file_get_contents($fileError);
            $res = trim($res);
            $res = str_replace("\n", "", $res);

            $this->logger->INFO("cokeREPORT CMD " . $cmd . " response: $res ");

            if (strlen($res) > 0 || $res != "") {
                unlink($filePath_header);
                unlink($filePath_body);
                unlink($fileError);
                $this->logger->ERROR($res);
            }
            else {
                shell_exec("cat $filePath_header $filePath_body > $filePath");
                unlink($filePath_header);
                unlink($filePath_body);
                unlink($fileError);
                $paths[] = $filePath;
            }

            if(count($paths) > 0) {

                $fileName = "report_" . time('YmdHis').".tar.gz";;
                $root = "/var/www/html/coke-cron-job/reports/";
                $uploadFile = $root.$fileName;

                exec("cd $root && tar -zcvf $fileName *.csv --remove-files");

                $fileType = mime_content_type($uploadFile);

                $this->logger->INFO("GOT fileType $fileType to POST $uploadFile");

                $url = "http://api.southwell.io/mailWrapper/mail/send"; // request URL

                $header = array('Content-Type: multipart/form-data');

                $mails = explode(",",Config::emailCC);

                foreach ($mails as $to) {

					$this->logger->INFO("prepare $to ");

                    $postData = new stdClass();
                    $postData->recipient = $to;
                    $postData->cc = "stats@southwell.io";
                    $postData->message = "Fungua Mamili reports, generated at " . date('Y-m-d H:i:s');
                    $postData->subject = "Fungua Mamili Reports for " . date('Y-m-d', strtotime("-1 days"));
                    $postData->bcc = "philip.mudenyo@southwell.io";
                    $postData->file = new CurlFile($uploadFile, null, basename($uploadFile));

                    $httpRequest = curl_init();
                    curl_setopt($httpRequest, CURLOPT_URL, $url);
                    curl_setopt($httpRequest, CURLOPT_POST, true);
                    curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $postData);
                    curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($httpRequest, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, true);
                    $result = curl_exec($httpRequest);
                    $status = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE);
                    curl_close($httpRequest);

                    $this->logger->INFO(" $to $status $result");
                }

                unlink($uploadFile);
            }
        }

    }

    $job = new Job();
    $job->execute();
    die();
?>
