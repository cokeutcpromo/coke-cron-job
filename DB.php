<?php

    /**
     * this class defines database connections
     */
    class DB
    {
        /**
         * executes all queries
         */
        public static function executeFetchStatement($sql, $bindingParams = null, $line = null)
        {
            $t1 = Self::getmicrotime();
            $user_id = isset($_SESSION['userID']) ? $_SESSION['userID'] : 0;
            $logger = new MenuLogger(Config::loggingDir, Config::infoLog, Config::errorLog);
            //$logger->ALERT(" SQL $line user_id $user_id query $sql bindingParams ".count($bindingParams));

            try {
                $conn = new PDO('mysql:host=' . Config::dbHost . ';dbname=' . Config::dbName, Config::dbUser, base64_decode(Config::dbPassword));
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $stmt = $conn->prepare($sql);
                $stmt->execute($bindingParams);
                $results = array();
                while ($row = $stmt->fetchObject()) {
                    $results[] = $row;
                }

                $stmt = null;
                $conn = null;
                $t2 = Self::getmicrotime();
                $time = $t2 - $t1;
                //$logger->ALERT(" SQL $line user_id $user_id query response time in s $time response ".count($results));
                return $results;
            } catch (Exception $ex) {
                $logger->EXCEPTION(" SQL $line SQL $sql user_id $user_id " . $ex->getMessage() . " TRACE " . $ex->getTraceAsString());
                $stmt = null;
                $conn = null;
                exit(1);
            }
        }

        public static function executeFetchStatementMain($sql, $bindingParams = null, $line = null)
        {
            $t1 = Self::getmicrotime();
            $user_id = isset($_SESSION['userID']) ? $_SESSION['userID'] : 0;
            $logger = new MenuLogger(Config::loggingDir, Config::infoLog, Config::errorLog);
            //$logger->ALERT(" SQL $line user_id $user_id query $sql bindingParams ".count($bindingParams));

            try {
                $conn = new PDO('mysql:host=' . WritableDB::dbHost . ';dbname=' . WritableDB::dbName, WritableDB::dbUser, base64_decode(WritableDB::dbPassword));
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $stmt = $conn->prepare($sql);
                $stmt->execute($bindingParams);
                $results = array();
                while ($row = $stmt->fetchObject()) {
                    $results[] = $row;
                }

                $stmt = null;
                $conn = null;
                $t2 = Self::getmicrotime();
                $time = $t2 - $t1;
                $logger->ALERT(" SQL $line user_id $user_id query response time in s $time response " . count($results));
                return $results;
            } catch (Exception $ex) {
                $logger->EXCEPTION(" SQL $line SQL $sql  user_id $user_id " . $ex->getMessage() . " TRACE " . $ex->getTraceAsString());
                $stmt = null;
                $conn = null;
                exit(1);
            }
        }

        public static function executeInsertStatement($sql, $bindingParams = null, $line = null)
        {
            $t1 = Self::getmicrotime();
            $user_id = isset($_SESSION['userID']) ? $_SESSION['userID'] : 0;
            $logger = new MenuLogger(Config::loggingDir, Config::infoLog, Config::errorLog);
            //$logger->ALERT(" SQL $line user_id $user_id query $sql bindingParams ".count($bindingParams));

            try {
                $conn = new PDO('mysql:host=' . WritableDB::dbHost . ';dbname=' . WritableDB::dbName, WritableDB::dbUser, base64_decode(WritableDB::dbPassword));
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $stmt = $conn->prepare($sql);
                $stmt->execute($bindingParams);
                $lastInsertId = $conn->lastInsertId();
                $stmt = null;
                $conn = null;
                $t2 = Self::getmicrotime();
                $time = $t2 - $t1;
                //$logger->ALERT(" SQL $line user_id $user_id response time in s $time query response insertedID $lastInsertId");
                return $lastInsertId;
            } catch (Exception $ex) {
                $logger->EXCEPTION(" SQL $line SQL $sql  user_id $user_id " . $ex->getMessage() . " TRACE " . $ex->getTraceAsString());
                $stmt = null;
                $conn = null;
                exit(1);
            }
        }

        public static function executeUpdateStatement($sql, $bindingParams = null, $line = null)
        {
            $t1 = Self::getmicrotime();
            $user_id = isset($_SESSION['userID']) ? $_SESSION['userID'] : 0;
            $logger = new MenuLogger(Config::loggingDir, Config::infoLog, Config::errorLog);
            //$logger->ALERT(" SQL $line user_id $user_id query $sql bindingParams ".count($bindingParams));

            try {
                $conn = new PDO('mysql:host=' . WritableDB::dbHost . ';dbname=' . WritableDB::dbName, WritableDB::dbUser, base64_decode(WritableDB::dbPassword));
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $stmt = $conn->prepare($sql);
                $stmt->execute($bindingParams);
                $rowCount = $stmt->rowCount();
                $stmt = null;
                $conn = null;
                $t2 = Self::getmicrotime();
                $time = $t2 - $t1;
                //$logger->ALERT(" SQL $line user_id $user_id response time in s $time query response rowCount $rowCount");
                return $rowCount;
            } catch (Exception $ex) {
                $logger->EXCEPTION(" SQL $line SQL $sql  user_id $user_id " . $ex->getMessage() . " TRACE " . $ex->getTraceAsString());
                $stmt = null;
                $conn = null;
                exit(1);
            }
        }

        public static function executeBatch($sql, $load = array(), $line = null)
        {
            $t1 = Self::getmicrotime();
            $logger = new MenuLogger(Config::loggingDir, Config::infoLog, Config::errorLog);
            //$logger->ALERT(" SQL BATCH user_id $user_id query $sql bindingParams ".count($load));

            try {
                $conn = new PDO('mysql:host=' . WritableDB::dbHost . ';dbname=' . WritableDB::dbName, WritableDB::dbUser, base64_decode(WritableDB::dbPassword));
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $conn->beginTransaction();
                $stmt = $conn->prepare($sql);
                $rowCount = 0;

                foreach ($load as $key => $value) {
                    $status = $stmt->execute($value);
                    $rowCount = $rowCount + $status;
                }

                $conn->commit();
                $stmt = null;
                $conn = null;
                $t2 = Self::getmicrotime();
                $time = $t2 - $t1;
                //$logger->ALERT("SQL $line user_id $user_id response time in s $time query response rowCount $rowCount");
                return $rowCount;
            } catch (Exception $ex) {
                $logger->EXCEPTION(" SQL $line SQL $sql " . $ex->getMessage() . " TRACE " . $ex->getTraceAsString());
                $stmt = null;
                $conn = null;
                exit(1);
            }
        }

        /**
         * @param     string  $sql
         * @param array $load
         * @param null  $line
         *
         * @return int
         */
        public static function batchUpdate($sql, $bindingParams = array(), $line = null)
        {
            $logger = new MenuLogger(Config::loggingDir, Config::infoLog, Config::errorLog);

            try {
                $conn = new PDO('mysql:host=' . WritableDB::dbHost . ';dbname=' . WritableDB::dbName, WritableDB::dbUser, base64_decode(WritableDB::dbPassword));
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $conn->beginTransaction();
                $stmt = $conn->prepare($sql);
                $rowCount = 0;

                foreach ($bindingParams as $value) {

                    //$logger->INFO(" SQL batchUpdate value ".json_encode($value));
                    $stmt->execute($value);
                    $rowCount = $stmt->rowCount() + $rowCount;
                }

                $conn->commit();
                $stmt = null;
                $conn = null;

                return $rowCount;
            } catch (Exception $ex) {
                $logger->EXCEPTION(" SQL $line SQL $sql " . $ex->getMessage() . " TRACE " . $ex->getTraceAsString());
                $stmt = null;
                $conn = null;
                exit(1);
            }
        }

        public static function fetchOne($sql, $bindingParams = null, $line = null)
        {
            $t1 = Self::getmicrotime();
            $user_id = isset($_SESSION['userID']) ? $_SESSION['userID'] : 0;
            $logger = new MenuLogger(Config::loggingDir, Config::infoLog, Config::errorLog);
            //$logger->ALERT(" SQL $line user_id $user_id query $sql bindingParams ".count($bindingParams));

            try {
                $conn = new PDO('mysql:host=' . Config::dbHost . ';dbname=' . Config::dbName, Config::dbUser, base64_decode(Config::dbPassword));
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $stmt = $conn->prepare($sql);
                $stmt->execute($bindingParams);
                $results = false;
                while ($row = $stmt->fetchObject()) {
                    $results = $row;
                }

                $stmt = null;
                $conn = null;
                $t2 = Self::getmicrotime();
                $time = $t2 - $t1;
                //$logger->ALERT(" SQL $line user_id $user_id query response time in s $time response ".count($results));
                return $results;
            } catch (Exception $ex) {
                $logger->EXCEPTION(" SQL $line SQL $sql user_id $user_id " . $ex->getMessage() . " TRACE " . $ex->getTraceAsString());
                $stmt = null;
                $conn = null;
                exit(1);
            }
        }

        /** * clean variable string * * @param type $text * @return type */
        public static function cleanString($text)
        {
            $utf8 = array('/[áàâãªä]/u' => 'a', '/[ÁÀÂÃÄ]/u' => 'A', '/[ÍÌÎÏ]/u' => 'I', '/[íìîï]/u' => 'i', '/[éèêë]/u' => 'e', '/[ÉÈÊË]/u' => 'E', '/[óòôõºö]/u' => 'o', '/[ÓÒÔÕÖ]/u' => 'O', '/[úùûü]/u' => 'u', '/[ÚÙÛÜ]/u' => 'U', '/ç/' => 'c', '/Ç/' => 'C', '/ñ/' => 'n', '/Ñ/' => 'N', '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
                          '/[’‘‹›‚]/u'  => ' ', // Literally a single quote
                          '/[“”«»„]/u'  => ' ', // Double quote
                          '/ /'         => ' ', // nonbreaking space (equiv. to 0x160)
            );

            return preg_replace(array_keys($utf8), array_values($utf8), $text);
        }

        public static function getmicrotime()
        {

            list ($msec, $sec) = explode(" ", microtime());
            return ((float)$msec + (float)$sec);
        }
    }

?>
