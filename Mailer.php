<?php
    /**
     * Created by PhpStorm.
     * User: mukuha
     * Date: 11/14/17
     * Time: 10:42 AM
     */

    class Mailer
    {
        public $subject = "";
        private $message = "";
        public $to = "";
        public $cc = "";
        public $bcc = "";
        public $hasTable = false;
        private $rows = array();
        public $headers = "";
        public $footers = "";

        /**
         * @param string $message
         */
        public function setMessage($message)
        {
            $this->message = $this->message . $message;
        }

        /**
         * @param string $bcc
         */
        public function setBcc($bcc)
        {
            $this->bcc = $bcc;
        }

        /**
         * @param string $cc
         */
        public function setCc($cc)
        {
            $this->cc = $cc;
        }

        /**
         * @param string $rows
         */

        public function setRows($rows)
        {
            $this->rows[] = $rows;
        }

        /**
         * @param bool $hasTable
         */
        public function setHasTable($hasTable)
        {
            $this->hasTable = $hasTable;
        }

        /**
         * @param string $to
         */
        public function setTo($to)
        {
            $this->to = $to;
        }

        /**
         * @param string $subject
         */
        public function setSubject($subject)
        {
            $this->subject = $subject;
        }

        /**
         * @return string
         */
        public function getMessage()
        {
            return $this->message . $this->headers . implode("", $this->rows) . $this->footers;

        }

        public function send() {

            $url = "http://api.southwell.io/mailer/";

            $post = array(

                "emailMessage" => $this->getMessage(),
                "emailSubject" => $this->subject,
                "recipient"=> $this->to,
                "recipient_to_cc" => $this->cc,
                "recipient_to_bcc" => $this->bcc,
                "emailAttach" => "",
                "smtpConfigs" => array(
                    "smtpHost"=>"",
                    "smtpPort"=>"",
                    "smtpUser"=>"",
                    "smtpPass"=>"",
                ),
                "status" => "1"
            );

            return $this->makeHttpRequest($url, $post);
        }

        /**
         * @param string $url
         * @param array $data
         *
         * @return string
         */

        public function makeHttpRequest($url, $data) {

            if (is_array($data) || is_object($data)) {
                $postData = json_encode($data);
            } else {
                $postData = $data;
            }

            $httpRequest = curl_init($url);
            curl_setopt($httpRequest, CURLOPT_POST, 1);
            curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($postData))
            );
            $response = curl_exec($httpRequest);
            $status = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE);
            curl_close($httpRequest);

            return "response $response status $status";
        }
    }